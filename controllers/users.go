package controllers

import (
	"fmt"
	"net/http"

	"lenslocked.com/views"
)

// Users struct ...
type Users struct {
	NewView *views.View
}

// NewUsers is used to create a new User controller. This function will panic
// if the templates are not parsed correctly and should only be used during
// initial setup.
func NewUsers() *Users {
	return &Users{
		NewView: views.NewView("bootstrap", "users/new"),
	}
}

// New method is used to render the form where a user can create a
// new user account.
//
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	if err := u.NewView.Render(w, nil); err != nil {
		panic(err)
	}
}

// Create method is used to process the signup form when a user submits it.
// This is used to create a new user account.
//
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var form SignupForm

	if err := parseForm(r, &form); err != nil {
		panic(err)
	}

	fmt.Fprintln(w, form)
}

// SignupForm struct ...
type SignupForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}
