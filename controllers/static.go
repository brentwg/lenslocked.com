package controllers

import (
	"lenslocked.com/views"
)

// NewStatic function ...
func NewStatic() *Static {
	return &Static{
		Home:    views.NewView("bootstrap", "static/home"),
		Contact: views.NewView("bootstrap", "static/contact"),
		Faq:     views.NewView("bootstrap", "static/faq"),
	}
}

// Static struct ...
type Static struct {
	Home    *views.View
	Contact *views.View
	Faq     *views.View
}
